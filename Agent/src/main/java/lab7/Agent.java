package lab7;
import java.lang.instrument.Instrumentation;

public class Agent {
    private static Instrumentation instrumentation;
    public static void premain(String agentArgument, Instrumentation instrumentation) {
        System.out.println("Agent Counter");
        instrumentation.addTransformer(new ClassTransformer());
        Agent.instrumentation = instrumentation;
    }
    public static long getSize(Object obj) {
        if (instrumentation == null) {
            throw new IllegalStateException("Agent not initialised");
        }
        return instrumentation.getObjectSize(obj);
    }
}