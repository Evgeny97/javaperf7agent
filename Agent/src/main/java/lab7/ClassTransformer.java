package lab7;

import java.lang.instrument.ClassFileTransformer;
import java.security.ProtectionDomain;

public class ClassTransformer implements ClassFileTransformer {

    private static int count = 0;
    private static int totalSize = 0;

    @Override
    public byte[] transform(ClassLoader loader, String className, Class<?> classBeingRedefined,
                            ProtectionDomain protectionDomain, byte[] classfileBuffer) {
        long size = Agent.getSize(classfileBuffer);
        totalSize+=size;
        System.out.println(String.format("loaded %d classes, size: %d, length %d, total size %d", ++count, size,
                classfileBuffer.length,totalSize));
        return classfileBuffer;
    }
}